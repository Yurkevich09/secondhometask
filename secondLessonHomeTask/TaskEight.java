

package secondLessonHomeTask;

public class TaskEight {
    public static void main (String[] args){
        // Задание №8. Переделать задание 6 и 7 с использованием switch-case
        // часть 1.Определить четверть часа по переменной min.
        int n = 0, min = 13, day = 25;
        switch (min/10){
            case 0: switch (min%10){
                case 0: case 1: case 2: case 3: case 4: case 5:
                case 6: case 7: case 8: case 9: n = 1; break;
            } break;
            case 1: switch (min%10){
                case 0: case 1: case 2: case 3: case 4: n = 1; break;
                case 5: case 6: case 7: case 8: case 9: n = 2; break;
            } break;
            case 2: switch (min%10){
                case 0: case 1: case 2: case 3: case 4: case 5:
                case 6: case 7: case 8: case 9: n = 2; break;
            } break;
            case 3: switch (min%10){
                case 0: case 1: case 2: case 3: case 4: case 5:
                case 6: case 7: case 8: case 9: n = 3; break;
            } break;
            case 4: switch (min%10){
                case 0: case 1: case 2: case 3: case 4: n = 3; break;
                case 5: case 6: case 7: case 8: case 9: n = 4; break;
            } break;
            case 5: switch (min%10){
                case 0: case 1: case 2: case 3: case 4: case 5:
                case 6: case 7: case 8: case 9: n = 4; break;
            } break;
        }
        System.out.println("№8, часть 1. Переменная " + min + " соответствует " + n + " четверти часа");

        // часть 2.Определить какой декаде соотвествует переменная day
        switch (day/10){
            case 0: n = 1; break;
            case 1:	switch (day%10){
                case 0: n = 1; break;
                default: n = 2; break;
            } break;
            case 2:	switch (day%10){
                case 0: n = 2; break;
                default: n = 3; break;
            } break;
            case 3: n = 3; break;
        }
        System.out.println("№8, часть 2. Переменная " + day + " соответствует " + n + " декаде");
    }
}
