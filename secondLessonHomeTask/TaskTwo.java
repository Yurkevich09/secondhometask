

package secondLessonHomeTask;

public class TaskTwo {
    public static void main (String[] args){
        // Задание №2. Вывести количество максимальных чисел из задания 1.
        int min, n = 0;
        int q1 = 152, q2 = 64, q3 = 104, q4 = 352;
        if (q1 < q2) min = q1;
        else min = q2;
        if (min > q3) min = q3;
        if (min > q4) min = q4;
        System.out.println("№1. " + min + " - наименьшее число из: " + q1 + " " + q2 + " " + q3 + " " + q4);
        if (min < q1) n++;
        if (min < q2) n++;
        if (min < q3) n++;
        if (min < q4) n++;
        System.out.println("№2. Количество максимальных чисел больших " + min + " : " + n);
    }
}
