package secondLessonHomeTask;

public class TaskThree {
    public static void main (String[] args){
        // Задание №3. Вывести max и min значения из 5 чисел.
        int min, max;
        int e1 = 45, e2 = 205, e3 = 153, e4 = 471, e5 = 39;
        if (e1 < e2){
            min = e1;
            max = e2;
        }
        else {
            min = e2;
            max = e1;
        }
        if (min > e3) min = e3;
        else if (max < e3) max = e3;
        if (min > e4) min = e4;
        else if (max < e4) max = e4;
        if (min > e5) min = e5;
        else if (max < e5) max = e5;
        System.out.println("№3. " + min + " - наименьшее число, " + max + " - наибольшее число из: "
                + e1 + " " + e2 + " " + e3 + " " + e4 + " " + e5);
    }
}
