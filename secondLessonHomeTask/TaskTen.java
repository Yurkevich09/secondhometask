

package secondLessonHomeTask;

public class TaskTen {
    public static void main (String[] args){
        // Задание 10. Сравнить суммы первых и вторых трех цифр строки состоящей из 6 цифр.
        String v = "345543";
        int convert = Integer.valueOf(v);
        int summ1 = (convert/100000) + (convert%100000)/10000 + (convert%10000)/1000;
        int summ2 = (convert%1000)/100 + (convert%100)/10 + (convert%10);
        if (summ1 == summ2) v = "равна";
        else v = "не равна";
        System.out.println("№10. Сумма цифр " + convert/1000 + " и " + convert%1000 + " " + v);
    }
}
