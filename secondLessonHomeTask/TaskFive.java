

package secondLessonHomeTask;

public class TaskFive {
    public static void main (String[] args){
        // Задание №5. Определить пору года по месяцу.
        int mounth = 3;
        String name_Mounth;
        if (mounth > 2 && mounth < 6) name_Mounth = "Spring";
        else if (mounth > 5 && mounth < 9) name_Mounth = "Summer";
        else if (mounth > 8 && mounth < 12) name_Mounth = "Autumn";
        else name_Mounth = "Winter";
        System.out.println("№5. Номеру месяца " + mounth + " соответствует: " + name_Mounth);
    }
}
