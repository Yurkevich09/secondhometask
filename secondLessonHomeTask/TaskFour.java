

package secondLessonHomeTask;

public class TaskFour {
    public static void main (String[] args){
        // Задание №4. Сравнить имена двух человек.
        String s1 = "Ivan", s2 = "Igor";
        boolean name = s1.equals(s2);
        if (name) System.out.println("№4. Имя " + s1 + " соответствует имени " + s2);
        else System.out.println("№4. Имя " + s1 + " не соответствует имени " + s2);
    }
}
