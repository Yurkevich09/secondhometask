

package secondLessonHomeTask;

public class TaskSeven {
    public static void main (String[] args){
        // Задание №7. Определить какой декаде соотвествует переменная day.
        int n = 0, day = 25;
        if (day <= 10) n = 1;
        else if (day > 10 && day <= 20) n = 2;
        else n = 3;
        System.out.println("№7. Переменная " + day + " соответствует " + n + " декаде");
    }
}
