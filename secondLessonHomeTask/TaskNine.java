
package secondLessonHomeTask;

public class TaskNine {
    public static void main (String[] args){
        // Задание 9. Найти сумму цифр трехзначного числа.
        String v = "123";
        int convert = Integer.valueOf(v);
        int summ = convert/100 + (convert%100)/10 + ((convert%100)%10);
        System.out.println("№9. Сумма чисел строки " + v + " равна: " + summ);
    }
}
