package secondLessonHomeTask;

public class TaskOne {
    public static void main (String[] args){
        int min;
        // Задание №1. Наименьшее из ряда чисел
        int q1 = 152, q2 = 64, q3 = 104, q4 = 352;
        if (q1 < q2) min = q1;
        else min = q2;
        if (min > q3) min = q3;
        if (min > q4) min = q4;
        System.out.println("№1. " + min + " - наименьшее число из: " + q1 + " " + q2 + " " + q3 + " " + q4);
    }
}
